#
# $ pip3 show \
#     mkdocs \
#     mkdocs-material \
#     mkdocs-include-markdown-plugin \
#     plantuml-markdown \
#     | grep '\(Name\|Version\)'
#
# Install the dependencies with:
#
#   $ pip3 install --requirement ./requirements.txt
#

mkdocs==1.3.0
mkdocs-material==8.3.0
plantuml-markdown==3.5.2
